import { Component } from '@angular/core';
import { Employee } from './models/employee';
import { isNullOrUndefined } from 'util';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  employeeArray: Employee[] = [
    {id: 1, name: 'Ryan', country: 'USA'},
    {id: 2, name: 'Jose', country: 'MX'},
    {id: 3, name: 'Werner', country: 'GER'}
  ];

  selectedEmployee: Employee = {id: 0, name: '', country: ''};

  addOrEdit() {
    if (this.selectedEmployee.id === 0 || isNullOrUndefined(this.selectedEmployee.id)) {
      this.selectedEmployee.id = this.employeeArray.length + 1;
      this.employeeArray.push(this.selectedEmployee);
    }
    this.selectedEmployee = {id: 0, name: '', country: ''};
  }

  openEdit(employee: Employee) {
    this.selectedEmployee = employee;
  }

  delete() {
    if (confirm('Estas seguro de eliminar este registro?')) {
      this.employeeArray = this.employeeArray.filter(x => x !== this.selectedEmployee);
      this.selectedEmployee = {id: 0, name: '', country: ''};
    }
  }
}
